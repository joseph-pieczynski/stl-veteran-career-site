'use strict';

/**
 * Career Controller
 */

module.exports.controller = function (app) {
  app.get('/learn', function (req, res) {
    res.render('learn/learn', {
      url: req.url
    });
  });

  app.get('/learn/track2', function (req, res) {
    if (!req.user) {
      return res.redirect('/login');
    }
    res.render('learn/track2', {
      url: req.url
    });
  });

  app.get('/learn/track1', function (req, res) {
    if (!req.user) {
      return res.redirect('/login');
    }
    res.render('learn/track1', {
      url: req.url
    });
  });

  app.get('/learn/track3', function (req, res) {
    if (!req.user) {
      return res.redirect('/login');
    }
    res.render('learn/track3', {
      url: req.url
    });
  });
};
