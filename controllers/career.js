'use strict';

/**
 * Career Controller
 */

module.exports.controller = function (app) {
  app.get('/career', function (req, res) {
    res.render('career/career', {
      url: req.url
    });
  });

  app.get('/career/:c', function (req, res) {
    // var career = req.params.c;
    res.render('career/paths', {
      url: req.url
    });
  });
};
