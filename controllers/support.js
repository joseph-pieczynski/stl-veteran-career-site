'use strict';

/**
 * Support Controller
 */

module.exports.controller = function (app) {
  app.get('/support', function (req, res) {
    // var career = req.params.c;
    res.render('support/support', {
      url: req.url
    });
  });
};
