'use strict';

/**
 * Resources Controller
 */

module.exports.controller = function (app) {
  app.get('/resources/jobsearch', function (req, res) {
    res.render('resources/jobsearch', {
      url: req.url
    });
  });

  app.get('/resources/org', function (req, res) {
    res.render('resources/org', {
      url: req.url
    });
  });

  app.get('/resources/external', function (req, res) {
    res.render('resources/external', {
      url: req.url
    });
  });

};
